Working of plugin

1. Install the plugin.
2. Create Vehicle Categories in admin panel.
3. Create Vehicles and assign categories to them.
4. Use shortcode [vehicle_booking_form] to display the form in any admin page.
5. After submission email notifications will be sent to admin as well as user.
6. Admin can view bookings from admin panel.
7. A Thanks email will be sent on booking updation to customer.

 
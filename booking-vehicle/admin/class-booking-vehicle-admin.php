<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       vinod
 * @since      1.0.0
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/admin
 * @author     Vinod <vinodb@graycelltech.com>
 */
class Booking_Vehicle_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Booking_Vehicle_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Booking_Vehicle_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/booking-vehicle-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Booking_Vehicle_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Booking_Vehicle_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/booking-vehicle-admin.js', array( 'jquery' ), $this->version, false );
	}
	
	
	/** Function to create vehicle  **/
	public function create_vehicle() {
     
	   $labels = array(
		'name'               => _x( 'Vehicle', 'post type general name' ),
		'singular_name'      => _x( 'Vehicle', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
		'add_new_item'       => __( 'Add New Vehicle' ),
		'edit_item'          => __( 'Edit Vehicle' ),
		'new_item'           => __( 'New Vehicle' ),
		'all_items'          => __( 'All Vehicles' ),
		'view_item'          => __( 'View Vehicle' ),
		'search_items'       => __( 'Search Vehicle' ),
		'not_found'          => __( 'No Vehicle found' ),
		'not_found_in_trash' => __( 'No Vehicle found in the Trash' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Vehicles',
		'taxonomies'         => array('category' )
	  );
	  $args = array(
		'labels'        => $labels,
		'description'   => '',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => true,
	  );
      register_post_type( 'vehicle', $args ); 
	}
	
	

 
  //function to register vehicle type 
 
  public function create_vehicle_type() {
 
	  $labels = array(
		'name' => _x( 'Vehicle Type', 'taxonomy general name' ),
		'singular_name' => _x( 'Vehicle Type', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Vehicle Type' ),
		'all_items' => __( 'All Vehicle Types' ),
		'parent_item' => __( 'Parent Vehicle Type' ),
		'parent_item_colon' => __( 'Parent Vehicle Type:' ),
		'edit_item' => __( 'Edit Vehicle Type' ), 
		'update_item' => __( 'Update Vehicle Type' ),
		'add_new_item' => __( 'Add New Vehicle Type' ),
		'new_item_name' => __( 'New Vehicle Type' ),
		'menu_name' => __( 'Vehicle Types' ),
	  );    
	 
	// Now register the taxonomy
	  register_taxonomy('vehicle_type',array('vehicle'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_in_rest' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'vehicle_type' ),
	  ));
 
  }
	
	
	/** Create Custom category for vehicles 
   public function create_vehicle_type2() {
    $args = array(
        // All the other args
        'taxonomies'          => array( 'vehicle_type', 'category' ),
    );

    register_post_type( 'vehicle', $args );
}
	**/
	
	
	
	// 
  
// function to create price box in vehicle
 public function create_vehicle_price_box() {
    $screens = [ 'vehicle' ];
        add_meta_box(
            'vehicle_starting_price_id',                 // Unique ID
            'Vehicle Starting Price',      // Box title
            'vehicle_price_box_2120',  // Content callback, must be of type callable
            'vehicle'                            // Post type
        );
}


// Remove comment section from vehicle
function vehicle_comments_open( $open, $post_id ) {
    $post_type = get_post_type( $post_id );
    if ( $post_type == 'vehicle' ) {
        return false;
    }
	return true;
}




// functions to save vehicle price box	
public function save_vehicle_price_box( $post_id ) {
    if ( array_key_exists( 'starting_price', $_POST ) ) {
        update_post_meta(
            $post_id,
            'vehicle_starting_price_2120',
            $_POST['starting_price']
        );
    }
}

	
	

}

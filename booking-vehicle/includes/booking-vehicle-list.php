<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       vinod
 * @since      1.0.0
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 * @author     Vinod <vinodb@graycelltech.com>
 */
class SP_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $booking_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}

   public static function set_screen( $status, $option, $value ) {
	return $value;
}

	public function plugin_menu() {

		$hook = add_submenu_page(
			'edit.php?post_type=vehicle',
			'Vehicle Bookings',
			'Vehicle Bookings',
			'manage_options',
			'vehicle_bookings',
			'get_vehicle_bookings'
		);

		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}
	
	
	public function screen_option() {

	$option = 'per_page';
	$args   = [
		'label'   => 'Bookings',
		'default' => 5,
		'option'  => 'bookings_per_page'
	];

	add_screen_option( $option, $args );

	$this->booking_obj = new Booking_Vehicle_List();
}

		 /** Singleton instance */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

}	




<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       vinod
 * @since      1.0.0
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 * @author     Vinod <vinodb@graycelltech.com>
 */
class Booking_Vehicle_List extends WP_List_Table {

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Booking', 'sp' ), //singular name of the listed records
			'plural'   => __( 'Bookings', 'sp' ), //plural name of the listed records
			'ajax'     => false //should this table support ajax?

		] );

	}
	
	
	
	public static function get_vehicle_bookings( $per_page = 5, $page_number = 1 ) {

		  global $wpdb;

		  $sql = "SELECT * FROM {$wpdb->prefix}vehicle_bookings";

		  if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		  }

		  $sql .= " LIMIT $per_page";

		  $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


		  $result = $wpdb->get_results( $sql, 'ARRAY_A' );

		  return $result;
  }
  
  
  /**
 * Delete a customer record.
 *
 * @param int $id customer ID
 */
	public static function delete_vehicle_bookings( $id ) {
	  global $wpdb;

	  $wpdb->delete(
		"{$wpdb->prefix}vehicle_bookings",
		[ 'ID' => $id ],
		[ '%d' ]
	  );
	}
	
	
	/**
 * Returns the count of records in the database.
 *
 * @return null|string
 */
	public static function record_count() {
	  global $wpdb;

	  $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}vehicle_bookings";

	  return $wpdb->get_var( $sql );
	}
	
	
	/** Text displayed when no customer data is available */
		public function no_items() {
		  _e( 'No Bookings avaliable.', 'sp' );
		}
		
		
	/**
 * Method for name column
 *
 * @param array $item an array of DB data
 *
 * @return string
 */
	function column_first_name( $item ) {

	  // create a nonce
	  $delete_nonce = wp_create_nonce( 'delete_vehicle_bookings' );
      $edit_nonce = wp_create_nonce( 'edit_vehicle_bookings_'.$item['id'] );
	  $title = '<strong>' . $item['first_name'] . '</strong>';
	  $actions = [
		'edit' => sprintf( '<a href="?page=%s&action=%s&booking=%s&_wpnonce=%s">Edit</a>', esc_attr( $_REQUEST['page'] ), 'edit_vehicle_bookings', absint( $item['id'] ), $edit_nonce ),
		'delete' => sprintf( '<a href="?page=%s&action=%s&booking=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['id'] ), $delete_nonce )
	  ];

	  return $title . $this->row_actions( $actions );
	}	
	
	
	/**
 * Render a column when no column specific method exists.
 *
 * @param array $item
 * @param string $column_name
 *
 * @return mixed
 */
	public function column_default( $item, $column_name ) {
	  switch ( $column_name ) {
		case 'first_name':
		case 'last_name':
		case 'email':
		case 'phone':
		  return $item[ $column_name ];
		
        case 'status':
		  return ($item[ $column_name ]==0) ? "Pending" : "Completed";		
		  
		case 'booking_date':
		  return date("jS F Y",strtotime($item[ $column_name ]));

        case 'vehicle': 
		  $post = get_post($item[ $column_name ]);
		  return $post->post_title;		  
		  
		default:
		  return print_r( $item, true ); //Show the whole array for troubleshooting purposes
	  }
	}
	
	
	/**
 * Render the bulk edit checkbox
 *
 * @param array $item
 *
 * @return string
 */
	function column_cb( $item ) {
	  return sprintf(
		'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
	  );
	}
	
	
	/**
 *  Associative array of columns
 *
 * @return array
 */
	function get_columns() {
	  $columns = [
		'cb'      => '<input type="checkbox" />',
		'first_name'    => __( 'First Name', 'sp' ),
		'last_name'    => __( 'Last Name', 'sp' ),
		'email' => __( 'Email', 'sp' ),
		'phone'    => __( 'Phone', 'sp' ),
		'vehicle' => __( 'Vehicle', 'sp' ),
		'booking_date' => __( 'Booking Date', 'sp' ),
		'status' => __( 'Status', 'sp' ),
	  ];

	  return $columns;
	}
	
	
	/**
 * Columns to make sortable.
 *
 * @return array
 */
	public function get_sortable_columns() {
	  $sortable_columns = array(
		'first_name' => array( 'first_name', true ),
		'last_name' => array( 'last_name', false )
	  );

	  return $sortable_columns;
	}
	
	
	/**
 * Returns an associative array containing the bulk action
 *
 * @return array
 */
	public function get_bulk_actions() {
	  $actions = [
		'bulk-delete' => 'Delete'
	  ];

	  return $actions;
	}
	
	
	/**
 * Handles data query and filter, sorting, and pagination.
 */
	public function prepare_items() {

	  $this->_column_headers = $this->get_column_info();

	  /** Process bulk action */
	  $this->process_bulk_action();

	  $per_page     = $this->get_items_per_page( 'bookings_per_page', 5 );
	  $current_page = $this->get_pagenum();
	  $total_items  = self::record_count();

	  $this->set_pagination_args( [
		'total_items' => $total_items, //WE have to calculate the total number of items
		'per_page'    => $per_page //WE have to determine how many items to show on a page
	  ] );


	  $this->items = self::get_vehicle_bookings( $per_page, $current_page );
	}
	
	
	
	public function process_bulk_action() {
      
	  //Detect when a bulk action is being triggered...
	  if ( 'delete' === $this->current_action() ) {

		// In our file that handles the request, verify the nonce.
		$nonce = esc_attr( $_REQUEST['_wpnonce'] );

		if ( ! wp_verify_nonce( $nonce, 'delete_vehicle_bookings' ) ) {
		  die( 'Go get a life script kiddies' );
		}
		else {
		  self::delete_vehicle_bookings( absint( $_GET['booking'] ) );

		  wp_redirect( esc_url( add_query_arg() ) );
		  exit;
		}

	  }

	  // If the delete bulk action is triggered
	  if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		   || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
	  ) {

		$delete_ids = esc_sql( $_POST['bulk-delete'] );

		// loop over the array of record IDs and delete them
		foreach ( $delete_ids as $id ) {
		  self::delete_vehicle_bookings( $id );

		}

		wp_redirect( esc_url( add_query_arg() ) );
		exit;
	  }
	}

}

<?php

// function to create vehicle booking form
// Use unique name not conflicted with others
 function create_vehicle_booking_form_2120() { 
  $nonce = wp_create_nonce('vehicle_booking_form_8647');
  $html = '<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';
  $html .= '<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>';
  $html .= "<form name='vehicle_booking_form' id='vehicle_booking_form'>";
  //$html .= "<label>First Name</label>";
  $html .= "<input type='text' name='first_name' placeholder='First Name' class='form-control' /><br/><br/>";
  $html .= "<input type='text' name='last_name' placeholder='Lsst Name' class='form-control' /><br/><br/>";
  $html .= "<input type='text' name='email' placeholder='Email' class='form-control' /><br/><br/>";
  $html .= "<input type='text' name='phone' placeholder='Phone' class='form-control' /><br/><br/>";
  $html .= "<input type='hidden' name='vehicle_code' id='vehicle_code' value= '$nonce' />";
  $html .= "<select name='vehicle_type' class='vehicle_type'>";
  
  $terms = get_terms( array(
    'taxonomy' => 'vehicle_type',
    'hide_empty' => false,
) );
  foreach($terms as $term)
   $html .= "<option value='$term->term_id'>$term->name</option>";
  $html .= "</select><br/><br/>";
  
  $html .= "<select name='vehicle' class='vehicles'>";
  $html .= "<option value=''>Select Vehicle</option>";
  $html .= "</select><br/><br/>";
  $html .= "<input type='text' name='starting_price' id='starting_price' placeholder='Starting Price' class='form-control' value='$value' /><br/><br/>";
  $html .= "<input type='text' name='booking_date' id='datepicker' placeholder='Booking Date' class='form-control' /><br/><br/>";
  $html .= "<input type='text' name='booking_time' id='timepicker' placeholder='Booking Time' class='form-control' /><br/><br/>";
  $html .= "<button type='button' name='submit' id='submit_vehicle_booking_2120'>Submit Booking</button><br/><br/>";
  $html .= "</form>";

  return $html;
}


// Add meta box for vehicle starting price
function vehicle_price_box_2120( $post ) {
    $value = get_post_meta( $post->ID, 'vehicle_starting_price_2120', true );
	if(empty($value))
	 $value = 0.00;	
	?>
    <label for="price">Starting Price</label>
    <input type="text" name="starting_price" value="<?php echo $value; ?>" />
    <?php
}

add_action( 'plugins_loaded', function () {
	SP_Plugin::get_instance();
} );


function get_vehicle_bookings()
{
  $booking_obj = new Booking_Vehicle_List();
  $booking_obj->prepare_items();
  $booking_obj->display(); 
}


function send_email_notification($to,$subject,$message,$headers)
  {
	  wp_mail($to,$subject,$message,$headers);
  }



?>

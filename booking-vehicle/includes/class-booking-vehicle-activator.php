<?php

/**
 * Fired during plugin activation
 *
 * @link       vinod
 * @since      1.0.0
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 * @author     Vinod <vinodb@graycelltech.com>
 */
class Booking_Vehicle_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
       global $wpdb;

      $table_name = $wpdb->prefix . "vehicle_bookings";

		$sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
		first_name varchar(100) NOT NULL,
		last_name varchar(100) NOT NULL,
		email varchar(100) NOT NULL,
		phone varchar(100) NOT NULL,
		vehicle int(11) NOT NULL,
		status tinyint(4) NOT NULL DEFAULT '1',
		booking_date datetime NOT NULL,
		created_date datetime NOT NULL,
		PRIMARY KEY id (id), 
		UNIQUE KEY id (id)
		);";

require_once( ABSPATH . "wp-admin/includes/upgrade.php" );
dbDelta( $sql );
	}

}

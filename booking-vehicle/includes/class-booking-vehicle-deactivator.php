<?php

/**
 * Fired during plugin deactivation
 *
 * @link       vinod
 * @since      1.0.0
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/includes
 * @author     Vinod <vinodb@graycelltech.com>
 */
class Booking_Vehicle_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


      $(function() {
		  var ajax_url;
	    $('.vehicle_type').change(function(){
			var ncode = $("#vehicle_code").val();
			var category = $(this).val();
	        jQuery.ajax(
			   {
				type: "post",
				dataType: "json",
				url: vehicle_booking_ajax_object.ajax_url,
				data: {category:category,ncode:ncode,action:'get_vehicles_by_category'},
				success: function(d){
				  if(d.status == 1) {
				   	$('.vehicles').html(d.html);
					$('.vehicles').trigger('change');
				  }
				}
			  });   
	   });


		$('.vehicles').change(function(){
			var ncode = $("#vehicle_code").val();
			var vehicle = $(this).val();
	        jQuery.ajax(
			   {
				type: "post",
				dataType: "json",
				url: vehicle_booking_ajax_object.ajax_url,
				data: {vehicle:vehicle,ncode:ncode,action:'get_starting_price_by_id'},
				success: function(d){
				  if(d.status == 1)
				   	$('#starting_price').val(d.price);  
				}
			  });   
	   });
	   
	   
	   $('#submit_vehicle_booking_2120').click(function(){
		    var formData = $("#vehicle_booking_form").serialize();
			var ncode = $("#vehicle_code").val();
			var vehicle = $(this).val();
	        jQuery.ajax(
			   {
				type: "post",
				dataType: "json",
				url: vehicle_booking_ajax_object.ajax_url,
				data: {ncode:ncode,action:'submit_vehicle_booking','form':formData},
				success: function(d){
				  if(d.status == 1)
				   	alert('booking has been submitted');
				}
			  });   
	   });
	   
	   
	   jQuery( "#datepicker" ).datepicker({ minDate: 0});
	   $('#timepicker').timepicker();

});
	 

})( jQuery );

<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       vinod
 * @since      1.0.0
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Booking_Vehicle
 * @subpackage Booking_Vehicle/public
 * @author     Vinod <vinodb@graycelltech.com>
 */
class Booking_Vehicle_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Booking_Vehicle_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Booking_Vehicle_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/booking-vehicle-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'timepickercss', plugin_dir_url( __FILE__ ) . 'css/jquery.timepicker.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Booking_Vehicle_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Booking_Vehicle_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/booking-vehicle-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'timepickerjs', plugin_dir_url( __FILE__ ) . 'js/jquery.timepicker.min', array( 'jquery' ), $this->version, false );
		
        wp_localize_script( $this->plugin_name, 'vehicle_booking_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}
	
	/** function to get posts by category **/
	public function get_vehicles_by_category()
    {
     $nonce = $_REQUEST['ncode'];
	 $category = $_REQUEST['category'];
	 $nonce2 = wp_create_nonce('vehicle_booking_form_8647');
	 $html = "";
	 if($nonce != $nonce2)
	  $status = 0;
     else {  
	  $args = array( 'posts_per_page' => -1, 'category' => $category7, 'orderby' => 'name', 'order' => 'ASC', 'post_type' => 'vehicle' ); 
	  $posts = get_posts($args);
      
	  foreach($posts as $post)
	  {
		$id =  $post->ID;
        $title = $post->post_title; 		
		$html .= "<option value='$id'>$title</option>";  
	  }
	  $status = 1;
	 }
	 
	 echo json_encode(array('status'=>$status,'html'=>$html));die;
    }
	
	
	
	/** function to get price by id **/
	public function get_starting_price_by_id()
    {
     $nonce = $_REQUEST['ncode'];
	 $vehicle = $_REQUEST['vehicle'];
	 $nonce2 = wp_create_nonce('vehicle_booking_form_8647');
	 $html = "";
	 if($nonce != $nonce2)
	  $status = 0;
     else {  
	  $post_meta = get_post_meta($vehicle,'vehicle_starting_price_2120');
      $price = array_shift($post_meta);
	  if(empty($price))
		$price = "0.00";  
	  
	  $status = 1;
	 }
	 
	 echo json_encode(array('status'=>$status,'price'=>$price));die;
    }
	
	
	
	
	/** function to submit booking **/
	public function submit_vehicle_booking()
    {
     $nonce = $_REQUEST['ncode'];
	 $form_data = $_REQUEST['form'];
	 
	 $nonce2 = wp_create_nonce('vehicle_booking_form_8647');
	 global $wpdb;
	 $html = "";
	 if($nonce != $nonce2)
	  $status = 0;
     else {  
	  $params = array();
      parse_str($form_data, $params);
      $first_name = $params['first_name'];	  
	  $last_name = $params['last_name'];	  
	  $email = $params['email'];	  
	  $phone = $params['phone'];
      $vehicle = $params['vehicle'];
	  $time = $params['booking_time'];
	  $booking_date = date("Y-m-d H:i:s",strtotime($params['booking_date']." ".$time));
      $table_name = $wpdb->prefix . "vehicle_bookings";
	  //$booking_date = date("Y-m-d H:i:s");
	  $created_date = date("Y-m-d H:i:s");
	  $query = "insert into $table_name (id,first_name,last_name,email,phone,vehicle,status,booking_date,created_date) values('','$first_name','$last_name','$email','$phone','$vehicle','0','$booking_date','$created_date')";
	  $wpdb->query($query);
	  $email_subject = "New Booking Submission";
	  $email_message = "<p>Dear Admin,</p>";
	  $email_message .= "<p>A new booking has been submitted.</p>";
	  $admin_email = get_option('admin_email');
	  $headers = array('Content-Type: text/html; charset=UTF-8');
	  send_email_notification($admin_email,$email_subject,$email_message,$headers);

      $email_subject = "Booking Submitted";
	  $email_message = "<p>Dear $first_name $last_name,</p>";
	  $email_message .= "<p>Your booking has been submitted and it is not yet approved by admin</p>";	 
	  send_email_notification($email,$email_subject,$email_message,$headers);
	  $status = 1;
	 }
	 
	 echo json_encode(array('status'=>$status));die;
    }
	

}
